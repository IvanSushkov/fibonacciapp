import json
import math

from django.http.response import JsonResponse
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt


def closed_form_fibonacci(number):
    SQRT_OF_FIVE = math.sqrt(5)
    ratio = (SQRT_OF_FIVE + 1) / 2
    return int(ratio ** number / SQRT_OF_FIVE + 0.5)


def validate_json_range(numbers):
    for number in numbers:
        try:
            int(number)
        except (TypeError, ValueError):
            return False
    return True


@csrf_exempt
@require_POST
def get_fibonacci_numbers(request):
    try:
        json_data = json.loads(request.body.decode())
    except ValueError:
        return JsonResponse({'msg': 'Can\'t decode json data'}, status=403)

    if not validate_json_range(json_data):
        return JsonResponse({'msg': 'Data must be a json array of integer numbers'}, status=403)

    result = []
    for number in json_data:
        try:
            calculated = closed_form_fibonacci(int(number))
        except (TypeError, ValueError):
            return JsonResponse({'msg': 'Data must be a json array of integer numbers'}, status=403)
        except OverflowError:
            return JsonResponse({'msg': '{} number is too large'.format(number)}, status=403)
        result.append(calculated)

    return JsonResponse(result, safe=False)
