from django.urls import path

from .views import get_fibonacci_numbers

app_name = 'restapi'

urlpatterns = [
    path('get-fibonacci/', get_fibonacci_numbers)
]